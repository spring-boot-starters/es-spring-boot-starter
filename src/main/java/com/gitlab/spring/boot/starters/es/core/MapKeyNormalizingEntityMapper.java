package com.gitlab.spring.boot.starters.es.core;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.databind.ser.std.StdKeySerializer;
import org.springframework.data.elasticsearch.core.EntityMapper;
import org.springframework.data.elasticsearch.core.geo.CustomGeoModule;

import java.io.IOException;
import java.util.Objects;

/**
 * A entity mapper that normalizes keys that occur
 * in maps having point notation, e.g. datasource.url -> datasource-url
 */
public class MapKeyNormalizingEntityMapper implements EntityMapper {
    private ObjectMapper objectMapper;

    public MapKeyNormalizingEntityMapper() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        objectMapper.registerModule(new CustomGeoModule());
        objectMapper.registerModule(new MapKeyNormalizerModule());
    }

    @Override
    public String mapToString(Object object) throws IOException {
        return objectMapper.writeValueAsString(object);
    }

    @Override
    public <T> T mapToObject(String source, Class<T> clazz) throws IOException {
        return objectMapper.readValue(source, clazz);
    }

    private class MapKeyNormalizerModule extends Module {
        @Override
        public String getModuleName() {
            return getClass().getName();
        }

        @Override
        public Version version() {
            return new Version(1, 0, 0, null, "com.gitlab.spring-boot-starters", "es-spring-boot-starter");
        }

        @Override
        public void setupModule(SetupContext context) {
            context.addKeySerializers(new KeyNormalizerSerializers());
        }
    }

    private class KeyNormalizerSerializers extends SimpleSerializers {
        KeyNormalizerSerializers() {
            addSerializer(Object.class, new KeyNormalizerSerializer());
        }
    }

    private class KeyNormalizerSerializer extends StdKeySerializer {
        @Override
        public void serialize(Object value,
                              JsonGenerator g,
                              SerializerProvider provider
        ) throws IOException {
            String newValue = Objects.toString(value, "").replace('.', '-');
            super.serialize(newValue, g, provider);
        }
    }
}
