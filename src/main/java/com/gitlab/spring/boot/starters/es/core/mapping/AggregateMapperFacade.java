package com.gitlab.spring.boot.starters.es.core.mapping;

import com.gitlab.spring.boot.starters.es.domain.AggregationImpl;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.SingleBucketAggregation;
import com.gitlab.spring.boot.starters.es.domain.Aggregation;

import java.util.Arrays;
import java.util.List;

/**
 * Aggregation mapper facade that recursively mapps aggregations
 */
public class AggregateMapperFacade implements AggregateMapper {
    private static final List<AggregateMapper> aggregateMappers = Arrays.asList(new SingleNumericValueAggregateMapper(),
                                                                                new MultiValueAggregateMapper());
    @Override
    public Class<?> getType() {
        return null;
    }

    @Override
    public Aggregation map(org.elasticsearch.search.aggregations.Aggregation aggregation) {
        return mapInternal(aggregation);
    }

    private Aggregation mapInternal(org.elasticsearch.search.aggregations.Aggregation aggregation) {
        AggregationImpl result = new AggregationImpl(aggregation.getName());

        // recursive multi bucket resolution (Buckets n:n Aggregations)
        if (MultiBucketsAggregation.class.isAssignableFrom(aggregation.getClass())) {
            for (MultiBucketsAggregation.Bucket bucket : ((MultiBucketsAggregation) aggregation).getBuckets()) {
                result.setDocCount(bucket.getKeyAsString(), bucket.getDocCount());

                Aggregations aggregations = bucket.getAggregations();
                for (org.elasticsearch.search.aggregations.Aggregation agg : aggregations) {
                    Aggregation tmp = mapInternal(agg);
                    String bucketKey = bucket.getKeyAsString();
                    result.addBucket(bucketKey, tmp);
                }
            }
        }

        // recursive single bucket resolution (Bucket 1:n Aggregations)
        if (SingleBucketAggregation.class.isAssignableFrom(aggregation.getClass())) {
            SingleBucketAggregation bucket = (SingleBucketAggregation) aggregation;
            result.setDocCount(bucket.getDocCount());

            Aggregations aggregations = bucket.getAggregations();
            for (org.elasticsearch.search.aggregations.Aggregation agg : aggregations) {
                Aggregation tmp = mapInternal(agg);
                result.addChild(tmp);
            }
        }

        // base mapping
        AggregateMapper aggregateMapper = getAggregateMapper(aggregation.getClass());
        if(aggregateMapper != null) {
            Aggregation tmp = aggregateMapper.map(aggregation);
            result = result.merge(tmp);
        }

        return result;
    }


    private AggregateMapper getAggregateMapper(Class<? extends org.elasticsearch.search.aggregations.Aggregation> aClass) {
        for (AggregateMapper aggregateMapper : aggregateMappers) {
            if(aggregateMapper.getType().isAssignableFrom(aClass)) {
                return aggregateMapper;
            }
        }
        return null;
    }
}
