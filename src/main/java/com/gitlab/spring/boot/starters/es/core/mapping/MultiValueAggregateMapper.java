package com.gitlab.spring.boot.starters.es.core.mapping;

import com.gitlab.spring.boot.starters.es.domain.Aggregation;
import com.gitlab.spring.boot.starters.es.domain.AggregationImpl;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.search.aggregations.metrics.InternalNumericMetricsAggregation;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
class MultiValueAggregateMapper implements AggregateMapper {

    private static final List<Metrics> MULTI_VALUE_METRICS = Metrics.getMultiValueMetrics();

    @Override
    public Class<?> getType() {
        return InternalNumericMetricsAggregation.MultiValue.class;
    }

    @Override
    public Aggregation map(org.elasticsearch.search.aggregations.Aggregation aggregation) {
        Assert.isInstanceOf(getType(), aggregation);

        InternalNumericMetricsAggregation.MultiValue value = (InternalNumericMetricsAggregation.MultiValue)aggregation;
        Map<String, String> valueMap = new HashMap<>();
        for (Metrics metrics : MULTI_VALUE_METRICS) {
            try {
                String valueAsString = value.valueAsString(metrics.getName());
                valueMap.put(metrics.name(), valueAsString);
            } catch (Exception e) {
                log.debug("No metric with name {}", metrics.name(), e);
            }
        }

        AggregationImpl result = new AggregationImpl(aggregation.getName());
        result.setValueMap(valueMap);
        return result;
    }
}
