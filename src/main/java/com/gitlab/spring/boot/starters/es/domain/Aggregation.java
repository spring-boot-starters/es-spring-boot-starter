package com.gitlab.spring.boot.starters.es.domain;

import java.util.Collection;
import java.util.Map;

/**
 * This class is part of es-spring-boot-starter
 * Created by stefan on 26.06.16.
 */
public interface Aggregation {
    String getName();

    Long getDocCount();

    Map<String, Long> getDocCountByBucket();

    Map<String, String> getValueMap();

    Collection<Aggregation> getChildren();

    Aggregation getChildByName(String name);

    Map<String, Aggregation> getBuckets();
}
