package com.gitlab.spring.boot.starters.es.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Defines the Aggregation result including, buckets, children ...
 */
@SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
@Data
@NoArgsConstructor
public class AggregationImpl implements Aggregation {
    private String name;
    private Long docCount;
    private Map<String, Long> docCountByBucket = new HashMap<>();
    private Map<String, String> valueMap = new HashMap<>();
    private Map<String, Aggregation> children = new HashMap<>();
    private Map<String, Aggregation> buckets = new HashMap<>();

    public AggregationImpl(String name) {
        this.name = name;
    }

    public AggregationImpl(String name, List<? extends MultiBucketsAggregation.Bucket> buckets ) {
        this.name = name;
        for (MultiBucketsAggregation.Bucket bucket : buckets) {
            this.docCountByBucket.put(bucket.getKeyAsString(), bucket.getDocCount());
        }
    }

    @Override
    public Collection<Aggregation> getChildren() {
        return children.values();
    }

    @Override
    public Aggregation getChildByName(String name) {
        return children.get(name);
    }

    public void addChild(Aggregation aggregation) {
        this.children.put(aggregation.getName(), aggregation);
    }

    public AggregationImpl merge(Aggregation tmp) {
        this.docCountByBucket = tmp.getDocCountByBucket();
        this.valueMap = tmp.getValueMap();
        this.docCount = tmp.getDocCount();
        return this;
    }

    public void addBucket(String bucketKey,
                          Aggregation tmp
    ) {
       this.buckets.put(bucketKey, tmp);
    }

    public void setDocCount(String bucketKey,
                            long docCount
    ) {
        this.docCountByBucket.put(bucketKey, docCount);
    }

    public void setDocCount(long docCount) {
        this.docCount = docCount;
    }

}
