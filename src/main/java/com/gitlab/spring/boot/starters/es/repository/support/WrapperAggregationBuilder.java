package com.gitlab.spring.boot.starters.es.repository.support;

import com.google.common.base.Charsets;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;

import java.io.IOException;

/**
 * Wrapper to translate json aggregate (a json string in es dsl syntax) into our actual es query object.
 *
 * @see CustomSearchQueryBuilder
 */
class WrapperAggregationBuilder extends AbstractAggregationBuilder {
    private final byte[] source;
    private final int offset;
    private final int length;

    /**
     * Creates a new builder.
     *
     * @param source json aggregation like {"myStatsAggregation":{"stats":{"field":"myFieldName"}}}
     */
    WrapperAggregationBuilder(String source) {
        super("wrapper", "aggs");
        this.source = source.getBytes(Charsets.UTF_8);
        this.offset = 0;
        this.length = this.source.length;
    }

    @Override
    public XContentBuilder toXContent(XContentBuilder builder,
                                      Params params
    ) throws IOException {

        try (XContentParser parser = XContentFactory.xContent(XContentType.JSON).createParser(source, offset, length)) {
            // need to jump to token FIELD_NAME (first token is START_OBJECT), token FIELD_NAME is the name of the
            // aggregation, e.g. myStatsAggregation
            parser.nextToken();
            parser.nextToken();
            builder.copyCurrentStructure(parser);
        }

        return builder;
    }
}
