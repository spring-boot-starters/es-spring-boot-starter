package com.gitlab.spring.boot.starters.es.core.mapping;

import com.gitlab.spring.boot.starters.es.domain.AggregationImpl;
import org.elasticsearch.search.aggregations.metrics.InternalNumericMetricsAggregation;
import org.elasticsearch.search.aggregations.metrics.NumericMetricsAggregation;
import com.gitlab.spring.boot.starters.es.domain.Aggregation;
import org.springframework.util.Assert;

import java.util.Collections;

/**
 * Aggregate mapper for single aggregates
 */
class SingleNumericValueAggregateMapper implements AggregateMapper {
    @Override
    public Class<?> getType() {
        return NumericMetricsAggregation.SingleValue.class;
    }

    @Override
    public Aggregation map(org.elasticsearch.search.aggregations.Aggregation aggregation) {
        Assert.isInstanceOf(getType(), aggregation);

        InternalNumericMetricsAggregation.SingleValue value = (InternalNumericMetricsAggregation.SingleValue)aggregation;
        String valueAsString = value.getValueAsString();

        AggregationImpl result = new AggregationImpl(aggregation.getName());
        result.setValueMap(Collections.singletonMap(Metrics.SINGLE_VALUE.getName(), valueAsString));
        return result;
    }
}
