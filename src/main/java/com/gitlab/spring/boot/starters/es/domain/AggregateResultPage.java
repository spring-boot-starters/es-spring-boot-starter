package com.gitlab.spring.boot.starters.es.domain;

import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.Set;

/**
 * This class is part of es-spring-boot-starter
 * Created by stefan on 26.06.16.
 */
public interface AggregateResultPage<T> extends Page<T> {
    Set<String> getHighlightFields();

    String getHighlightFieldFragment(String documentId,
                                     String highlightFieldName);

    Float getScore(String documentId);

    Collection<Aggregation> getAggregations();

    Aggregation getAggregationByName(String name);
}
