package com.gitlab.spring.boot.starters.es.core.query;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class HighlightField {
    String name;
    String[] preTags;
    String[] postTags;
    int fragmentSize = -1;
    int fragmentOffset = -1;
    int numOfFragments = -1;
    Boolean highlightFilter;
    String order;
    Boolean requireFieldMatch = true;
    int boundaryMaxScan = -1;
    char[] boundaryChars;
    String highlighterType;
    String fragmenter;
    String highlightQuery;
    Integer noMatchSize;
    String[] matchedFields;
    Integer phraseLimit;
    Map<String, Object> options;
    Boolean forceSource;

    public HighlightField(String name) {
        this.name = name;
    }

    public static Builder builder() {
        return new Builder();
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Builder {
        private List<HighlightField> fields = new ArrayList<>();
        private HighlightField current = null;

        public Builder addField(String name) {
            current = new HighlightField(name);
            this.fields.add(current);
            return this;
        }

        public List<HighlightField> getFields() {
            return Collections.unmodifiableList(fields);
        }
    }
}
