package com.gitlab.spring.boot.starters;

import com.gitlab.spring.boot.starters.es.client.TransportClientFactoryBean;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Custom implementation of the elastic search auto configuration.
 */
@Configuration
@ComponentScan
@EnableConfigurationProperties(ElasticsearchProperties.class)
public class ElasticsearchAutoConfiguration {

    private static final Map<String, String> DEFAULTS;

    static {
        Map<String, String> defaults = new LinkedHashMap<String, String>();
        defaults.put("http.enabled", Boolean.FALSE.toString());
        defaults.put("path.home", "./");
        DEFAULTS = Collections.unmodifiableMap(defaults);
    }

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private ElasticsearchProperties properties;

    /**
     * @return a new elasticsearch client bean
     * @see #createClient()
     */
    @Bean
    @ConditionalOnMissingBean
    public Client elasticsearchClient() {
        try {
            return createClient();
        }
        catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * @return a transport client if properties containing cluster nodes and a node client otherwise
     * @throws Exception
     */
    private Client createClient() throws Exception {
        if (StringUtils.hasLength(this.properties.getClusterNodes())) {
            return createTransportClient();
        }
        return createNodeClient();
    }

    /**
     * If you take away the ability to be able to handle master duties and take away the ability to hold data,
     * then you are left with a client node that can only route requests, handle the search reduce phase,
     * and distribute bulk indexing. Essentially, client nodes behave as smart load balancers.
     *
     * @return the new node client bean
     * @throws Exception
     */
    private Client createNodeClient() throws Exception {
        Settings.Builder settings = getNodeClientSettings();
        Node node = new NodeBuilder().settings(settings)
                .clusterName(this.properties.getClusterName()).node();
        return node.client();
    }

    private Settings.Builder getNodeClientSettings() {
        Settings.Builder settings = Settings.builder();
        for (Map.Entry<String, String> entry : DEFAULTS.entrySet()) {
            if (!this.properties.getProperties().containsKey(entry.getKey())) {
                settings.put(entry.getKey(), entry.getValue());
            }
        }
        settings.put(this.properties.getProperties());
        return settings;
    }

    /**
     * The TransportClient connects remotely to an Elasticsearch cluster using the transport module.
     * It does not join the cluster, but simply gets one or more initial transport addresses and communicates
     * with them in round robin fashion on each action (though most actions will probably be "two hop" operations).
     *
     * @return the new transport client bean
     * @throws Exception
     */
    private Client createTransportClient() throws Exception {
        TransportClientFactoryBean factory = new TransportClientFactoryBean();
        factory.setClusterNodes(this.properties.getClusterNodes());
        factory.setProperties(getTransportClientProperties());
        factory.afterPropertiesSet();
        TransportClient client = factory.getObject();
        return client;
    }

    private Properties getTransportClientProperties() {
        Properties properties = new Properties();
        properties.put("cluster.name", this.properties.getClusterName());
        properties.putAll(this.properties.getProperties());
        return properties;
    }
}
