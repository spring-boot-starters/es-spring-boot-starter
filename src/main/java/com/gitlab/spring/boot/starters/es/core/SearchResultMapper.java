package com.gitlab.spring.boot.starters.es.core;

import org.elasticsearch.action.search.SearchResponse;
import com.gitlab.spring.boot.starters.es.domain.SearchResultPage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.DefaultResultMapper;
import org.springframework.data.elasticsearch.core.EntityMapper;

/**
 * This implementation depends on the DefaultResultMapper.
 */
public class SearchResultMapper extends DefaultResultMapper {

    public SearchResultMapper(EntityMapper entityMapper) {
        super(entityMapper);
    }

    @Override
    public <T> Page<T> mapResults(SearchResponse response,
                                  Class<T> clazz,
                                  Pageable pageable
    ) {
        Page<T> page = super.mapResults(response, clazz, pageable);
        return new SearchResultPage<T>(page, response);
    }
}
