package com.gitlab.spring.boot.starters.es.repository;

import com.gitlab.spring.boot.starters.es.domain.AggregateResultPage;
import com.gitlab.spring.boot.starters.es.domain.SearchResultPage;
import com.gitlab.spring.boot.starters.es.core.query.SearchRequest;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Custom extended elasticsearch repository definition, which introduces
 * <ul>
 *     <li>a new search result page that contains scoring and highlighting info</li>
 *     <li>an aggregate method that returns a search result page enriched with aggregate data</li>
 * </ul>
 *
 * @param <T>
 */
@NoRepositoryBean
public interface ExtElasticsearchRepository<T>{
    /**
     * Call ext search.
     *
     * @param searchRequest that contains the query and aggregation definition
     * @return the SearchResultPage containing score and highlighting info
     */
    SearchResultPage<T> extSearch(SearchRequest searchRequest);

    AggregateResultPage<T> aggregate(SearchQuery searchQuery);

    /**
     * Call aggregate
     *
     * @param searchRequest that contains the query and aggregation definition
     * @return the AggregateResultPage containing aggregation results
     */
    AggregateResultPage<T> aggregate(SearchRequest searchRequest);
}
