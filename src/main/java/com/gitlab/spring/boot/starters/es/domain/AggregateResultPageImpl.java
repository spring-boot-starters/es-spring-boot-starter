package com.gitlab.spring.boot.starters.es.domain;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.*;
import java.util.function.Consumer;

/**
 * Page implementation that contains the actual search result as
 * well as results of user performed aggregations.
 */
@SuppressWarnings("FieldCanBeLocal")
public class AggregateResultPageImpl<T> implements AggregateResultPage<T> {

    private SearchResultPage searchResultPage;
    private final Map<String, Aggregation> aggregations = new HashMap<>();


    public AggregateResultPageImpl(SearchResultPage<T> page,
                                   List<Aggregation> aggregationsResult
    ) {
        this.searchResultPage = page;
        aggregationsResult.forEach((entry) -> aggregations.put(entry.getName(), entry));
    }

    @Override
    public Page<T> map(Converter converter) {
        return searchResultPage.map(converter);
    }

    @Override
    public Set<String> getHighlightFields() {
        return searchResultPage.getHighlightFields();
    }

    @Override
    public String getHighlightFieldFragment(String documentId,
                                            String highlightFieldName
    ) {
        return searchResultPage.getHighlightFieldFragment(documentId, highlightFieldName);
    }

    @Override
    public Float getScore(String documentId) {
        return searchResultPage.getScore(documentId);
    }

    @Override
    public void forEach(Consumer action) {
        searchResultPage.forEach(action);
    }

    @Override
    public int getTotalPages() {
        return searchResultPage.getTotalPages();
    }

    @Override
    public long getTotalElements() {
        return searchResultPage.getTotalElements();
    }

    @Override
    public int getNumber() {
        return searchResultPage.getNumber();
    }

    @Override
    public int getSize() {
        return searchResultPage.getSize();
    }

    @Override
    public int getNumberOfElements() {
        return searchResultPage.getNumberOfElements();
    }

    @Override
    public List<T> getContent() {
        return searchResultPage.getContent();
    }

    @Override
    public boolean hasContent() {
        return searchResultPage.hasContent();
    }

    @Override
    public Sort getSort() {
        return searchResultPage.getSort();
    }

    @Override
    public boolean isFirst() {
        return searchResultPage.isFirst();
    }

    @Override
    public boolean isLast() {
        return searchResultPage.isLast();
    }

    @Override
    public boolean hasNext() {
        return searchResultPage.hasNext();
    }

    @Override
    public boolean hasPrevious() {
        return searchResultPage.hasPrevious();
    }

    @Override
    public Pageable nextPageable() {
        return searchResultPage.nextPageable();
    }

    @Override
    public Pageable previousPageable() {
        return searchResultPage.previousPageable();
    }

    @Override
    public Iterator<T> iterator() {
        return searchResultPage.iterator();
    }

    @Override
    public Spliterator<T> spliterator() {
        return searchResultPage.spliterator();
    }

    @Override
    public Collection<Aggregation> getAggregations() {
        return aggregations.values();
    }

    @Override
    public Aggregation getAggregationByName(String name) {
        return aggregations.get(name);
    }
}
