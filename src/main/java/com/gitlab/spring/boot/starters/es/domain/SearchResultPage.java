package com.gitlab.spring.boot.starters.es.domain;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.highlight.HighlightField;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.function.Consumer;

/**
 * This class is part of ob-blacklist-search
 * Created by stefan on 14.05.16.
 */
@Slf4j
public class SearchResultPage<T> implements Page<T> {

    private final Page page;
    private final Map<String, Float> scoresByDocumentId = new HashMap<>();
    private final Map<String, Map<String, String>> highlightFieldsByDocumentId = new HashMap<>();

    public SearchResultPage(Page page,
                            SearchResponse response
    ) {
        this.page = page;
        for (SearchHit searchHit : response.getHits()) {
            addScore(searchHit);
            addHighlightedFields(searchHit);
        }
    }

    private void addHighlightedFields(SearchHit searchHit) {
        Map<String, HighlightField> highlightFields = searchHit.getHighlightFields();
        Map<String, String> highlightFieldsMap = new HashMap<>();
        this.highlightFieldsByDocumentId.put(searchHit.getId(), highlightFieldsMap);
        for (HighlightField highlightField : highlightFields.values()) {
            highlightFieldsMap.put(highlightField.getName(), Arrays.toString(highlightField.fragments()));
        }
    }

    private void addScore(SearchHit searchHit) {
        scoresByDocumentId.put(searchHit.getId(), searchHit.getScore());
    }

    @Override
    public int getTotalPages() {
        return page.getTotalPages();
    }

    @Override
    public long getTotalElements() {
        return page.getTotalElements();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Page<T> map(Converter converter) {
        return page.map(converter);
    }

    @Override
    public int getNumber() {
        return page.getNumber();
    }

    @Override
    public int getSize() {
        return page.getSize();
    }

    @Override
    public int getNumberOfElements() {
        return page.getNumberOfElements();
    }

    @Override
    public List<T> getContent() {
        return page.getContent();
    }

    @Override
    public boolean hasContent() {
        return page.hasContent();
    }

    @Override
    public Sort getSort() {
        return page.getSort();
    }

    @Override
    public boolean isFirst() {
        return page.isFirst();
    }

    @Override
    public boolean isLast() {
        return page.isLast();
    }

    @Override
    public boolean hasNext() {
        return page.hasNext();
    }

    @Override
    public boolean hasPrevious() {
        return page.hasPrevious();
    }

    @Override
    public Pageable nextPageable() {
        return page.nextPageable();
    }

    @Override
    public Pageable previousPageable() {
        return page.previousPageable();
    }

    @Override
    public Iterator<T> iterator() {
        return page.iterator();
    }

    @Override
    public void forEach(Consumer action) {
        page.forEach(action);
    }

    @Override
    public Spliterator<T> spliterator() {
        return page.spliterator();
    }

    public Float getScore(String documentId) {
        return scoresByDocumentId.get(documentId);
    }

    public Set<String> getHighlightFields() {
        return highlightFieldsByDocumentId.keySet();
    }

    public String getHighlightFieldFragment(String documentId, String highlightFieldName) {
        Map<String, String> highlightFieldsMap = highlightFieldsByDocumentId.get(documentId);
        String result = null;
        if(!CollectionUtils.isEmpty(highlightFieldsMap)) {
            result = highlightFieldsMap.get(highlightFieldName);
        }
        return result;
    }
}
