package com.gitlab.spring.boot.starters.es.repository.support;

import com.gitlab.spring.boot.starters.es.core.AggregateResultMapper;
import com.gitlab.spring.boot.starters.es.core.query.SearchRequest;
import com.gitlab.spring.boot.starters.es.domain.AggregateResultPage;
import com.gitlab.spring.boot.starters.es.domain.SearchResultPage;
import com.gitlab.spring.boot.starters.es.repository.ExtElasticsearchRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.elasticsearch.repository.support.AbstractElasticsearchRepository;
import org.springframework.data.elasticsearch.repository.support.ElasticsearchEntityInformation;
import org.springframework.util.ObjectUtils;

import java.io.Serializable;

/**
 * Implementation of the ExtElasticsearchRepository. <p>
 *     
 * Add configuration
 * <i><b>@EnableElasticSearchRepositories(repositoryBaseClass=ExtElasticsearchRepositoryImpl.class)</b></i> to enable this implementation
 * </p>
 *
 * @see ExtElasticsearchRepository
 * @param <T>
 */
@Slf4j
public class ExtElasticsearchRepositoryImpl<T> extends AbstractElasticsearchRepository<T, Serializable> implements ExtElasticsearchRepository<T> {

    public ExtElasticsearchRepositoryImpl() {
    }

    public ExtElasticsearchRepositoryImpl(ElasticsearchOperations elasticsearchOperations) {
        super(elasticsearchOperations);
    }

    public ExtElasticsearchRepositoryImpl(ElasticsearchEntityInformation<T, Serializable> metadata,
                                          ElasticsearchOperations elasticsearchOperations
    ) {
        super(metadata, elasticsearchOperations);
    }

    @Override
    public SearchResultPage<T> extSearch(SearchRequest searchRequest) {
        NativeSearchQuery nativeSearchQuery = buildNativeSearchQuery(searchRequest);

        return (SearchResultPage) elasticsearchOperations.queryForPage(nativeSearchQuery, getEntityClass());
    }

    @Override
    public AggregateResultPage<T> aggregate(SearchQuery searchQuery) {
        return (AggregateResultPage) elasticsearchOperations.queryForPage(searchQuery, getEntityClass(), new AggregateResultMapper());
    }

    @Override
    public AggregateResultPage aggregate(SearchRequest searchRequest) {
        NativeSearchQuery nativeSearchQuery = buildNativeSearchQuery(searchRequest);

        return (AggregateResultPage) elasticsearchOperations.queryForPage(nativeSearchQuery, getEntityClass(),
                                                                          new AggregateResultMapper());
    }

    private NativeSearchQuery buildNativeSearchQuery(SearchRequest searchRequest) {
        CustomSearchQueryBuilder customSearchQueryBuilder = new CustomSearchQueryBuilder();
        return customSearchQueryBuilder.withIndices(searchRequest.getIndices())
                .withTypes(searchRequest.getTypes())
                .withPageable(searchRequest.getFrom(), searchRequest.getSize())
                .withFields(searchRequest.getFields())
                .withFilter(searchRequest.getFilter())
                .withQuery(searchRequest.getQuery())
                .withAggregation(searchRequest.getAggs())
                .withHighlightFields(searchRequest.getHighlight())
                .build();
    }

    @Override
    protected String stringIdRepresentation(Serializable id) {
        return ObjectUtils.nullSafeToString(id);
    }


}
