package com.gitlab.spring.boot.starters.es.repository.support;

import ma.glasnost.orika.*;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.impl.GeneratedObjectFactory;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.highlight.HighlightBuilder;
import com.gitlab.spring.boot.starters.es.core.query.HighlightField;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * A custom implementation of the NativeSearchQueryBuilder. The primary
 * goal of this builder is mapping the HighlightField object to elasticsearch
 * HighlightBuilder.Field.
 *
 * @see ExtElasticsearchRepositoryImpl
 */
class CustomSearchQueryBuilder extends NativeSearchQueryBuilder {

    private final MapperFacade mapperFacade;

    public CustomSearchQueryBuilder() {
        MapperFactory mapperFactory = createMapperFactory();
        this.mapperFacade = mapperFactory.getMapperFacade();
    }

    /**
     * TODO: Test with single domain entity but different indices
     *
     * @param indices that will be queried
     */
    public CustomSearchQueryBuilder withIndices(List<String> indices) {
        withIndices(indices.toArray(new String[indices.size()]));
        return this;
    }

    /**
     * TODO: Test with single domain entity but different indices
     *
     * @param types that will be queried
     */
    public CustomSearchQueryBuilder withTypes(List<String> types) {
        withTypes(types.toArray(new String[types.size()]));
        return this;
    }

    /**
     * @param query to be used (json string in es dsl syntax)
     */
    public CustomSearchQueryBuilder withQuery(String query) {
        if(StringUtils.hasText(query)) {
            withQuery(QueryBuilders.wrapperQuery(query));
        }
        return this;
    }

    /**
     * @param aggs to be used (json string in es dsl syntax)
     */
    public CustomSearchQueryBuilder withAggregation(String aggs) {
        if(StringUtils.hasText(aggs)) {
            AbstractAggregationBuilder aggregation = new WrapperAggregationBuilder(aggs);
            addAggregation(aggregation);
        }
        return this;
    }

    /**
     * @param filter to be used (json string in es dsl syntax)
     */
    public CustomSearchQueryBuilder withFilter(String filter) {
        if(StringUtils.hasText(filter)) {
            withFilter(QueryBuilders.wrapperQuery(filter));
        }
        return this;
    }

    /**
     * @param fields to be returned by the query
     */
    public CustomSearchQueryBuilder withFields(List<String> fields) {
        withFields(fields.toArray(new String[fields.size()]));
        return this;
    }

    /**
     * @param highlightFields to be used
     */
    public CustomSearchQueryBuilder withHighlightFields(List<HighlightField> highlightFields) {
        if(!CollectionUtils.isEmpty(highlightFields)) {
            List<HighlightBuilder.Field> highlight = mapperFacade.mapAsList(highlightFields, HighlightBuilder.Field.class);
            withHighlightFields(highlight.toArray(new HighlightBuilder.Field[highlight.size()]));
        }
        return this;
    }

    /**
     * Adds a page request to the query
     *
     * @param from starting point
     * @param size number of items
     */
    public CustomSearchQueryBuilder withPageable(int from,
                                                 int size
    ) {
        withPageable(new PageRequest(from, size));
        return this;
    }


    private DefaultMapperFactory createMapperFactory() {
        DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().build();
        defaultMapperFactory.registerObjectFactory(createHighlightBuilderField(), HighlightBuilder.Field.class);
        defaultMapperFactory.classMap(HighlightField.class, HighlightBuilder.Field.class)
                .customize(new CustomMapper<HighlightField, HighlightBuilder.Field>() {
                    @Override
                    public void mapAtoB(HighlightField highlightField,
                                        HighlightBuilder.Field field,
                                        MappingContext context
                    ) {
                        if(StringUtils.hasText(highlightField.getHighlightQuery())) {
                            field.highlightQuery(QueryBuilders.wrapperQuery(highlightField.getHighlightQuery()));
                        }
                        super.mapAtoB(highlightField, field, context);
                    }
                })
                .register();
        return defaultMapperFactory;
    }

    private ObjectFactory createHighlightBuilderField() {
        return new GeneratedObjectFactory() {
            @Override
            public Object create(Object source,
                                 MappingContext mappingContext
            ) {
                String name = ((HighlightField) source).getName();
                return new HighlightBuilder.Field(name);
            }
        };
    }
}
