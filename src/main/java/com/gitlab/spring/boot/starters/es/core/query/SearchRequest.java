package com.gitlab.spring.boot.starters.es.core.query;

import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class SearchRequest {
    private String query;
    private List<String> indices = new ArrayList<>();
    private List<String> types = new ArrayList<>();
    private List<String> fields = new ArrayList<>();
    private int from = 0;
    private int size = 10;
    /**
     * Filter api applied to result documents after search
     */
    private String filter;
    private List<HighlightField> highlight;
    private String aggs;

    public void setIndices(String ... index) {
        indices = Arrays.asList(index);
    }

    public void setTypes(String ... type) {
        this.types = Arrays.asList(type);
    }

    public void setFields(String ... field) {
        this.fields = Arrays.asList(field);
    }

    public void addHighlight(HighlightField... highlightField) {
        this.highlight = Arrays.asList(highlightField);
    }

}
