package com.gitlab.spring.boot.starters.es.core.mapping;

import com.gitlab.spring.boot.starters.es.domain.Aggregation;

/**
 * Definition of known aggregate mappers
 */
public interface AggregateMapper {
    Class<?> getType();

    Aggregation map(org.elasticsearch.search.aggregations.Aggregation aggregation);
}
