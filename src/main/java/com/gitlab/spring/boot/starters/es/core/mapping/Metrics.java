package com.gitlab.spring.boot.starters.es.core.mapping;

import com.google.common.collect.FluentIterable;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

/**
 * Defines metrics as known by elasticsearch.
 */
enum  Metrics {
    SINGLE_VALUE("single_value"),
    COUNT("count"),
    SUM("sum"),
    MIN("min"),
    MAX("max"),
    AVG("avg"),
    SUM_OF_SQUARES("sum_of_squares"),
    VARIANCE("variance"),
    STD_DEVIATION("std_deviation"),
    STD_UPPER("std_upper"),
    STD_LOWER("std_lower");

    @Getter
    private final String name;

    Metrics(String name) {
        this.name = name;
    }

    public static List<Metrics> getMultiValueMetrics() {
        return FluentIterable.from(Arrays.asList(values()))
                .filter(metrics -> metrics != Metrics.SINGLE_VALUE).toList();
    }

    public static Metrics resolve(String name) {
        return Metrics.valueOf(name.toUpperCase());
    }
}
