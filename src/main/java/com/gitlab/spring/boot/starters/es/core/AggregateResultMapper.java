package com.gitlab.spring.boot.starters.es.core;

import com.gitlab.spring.boot.starters.es.core.mapping.AggregateMapper;
import com.gitlab.spring.boot.starters.es.core.mapping.AggregateMapperFacade;
import com.gitlab.spring.boot.starters.es.domain.AggregateResultPageImpl;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import com.gitlab.spring.boot.starters.es.domain.SearchResultPage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.DefaultResultMapper;

import java.util.ArrayList;
import java.util.List;

public class AggregateResultMapper extends DefaultResultMapper {

    private final AggregateMapper aggregateMapperFacade = new AggregateMapperFacade();

    @Override
    public <T> Page<T> mapResults(SearchResponse response,
                                  Class<T> clazz,
                                  Pageable pageable
    ) {
        final List<com.gitlab.spring.boot.starters.es.domain.Aggregation> aggregationsResult = new ArrayList<>();
        Aggregations aggregations = response.getAggregations();
        for (Aggregation aggregation : aggregations) {
            com.gitlab.spring.boot.starters.es.domain.Aggregation result = aggregateMapperFacade.map(aggregation);
            aggregationsResult.add(result);
        }

        Page<T> page = super.mapResults(response, clazz, pageable);
        SearchResultPage<T> searchResultPage = new SearchResultPage<>(page, response);
        return new AggregateResultPageImpl<T>(searchResultPage, aggregationsResult);
    }


}
